namespace ContactWeb.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ContactWebDbConfiguration : DbMigrationsConfiguration<ContactWeb.Models.ContactWebContext>
    {
        public ContactWebDbConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "ContactWeb.Models.ContactWebContext";
        }

        protected override void Seed(ContactWeb.Models.ContactWebContext context)
        {
            // 2fac849f-ca21-4fa9-bc72-eaa7092bb3a2
            // dcee1c5e-689c-4b76-acb7-75befb803e5b 
            context.Contacts.AddOrUpdate(
                p => p.Id,
                new Models.Contact
                {
                    Id = 1,
                    Birthday = new DateTime(1978, 01, 20),
                    City = "Scranton"
                                    ,
                    Email = "michaelscott@theofficeus.com",
                    FirstName = "Michael",
                    LastName = "Scott"
                                    ,
                    PhonePrimary = "123-456-7890",
                    PhoneSecondary = "234-567-8901",
                    State = "PA"
                                    ,
                    StreetAddress1 = "1725 Slough Avenue",
                    StreetAddress2 = "Suite 200 Dunder Mifflin Paper"
                                    ,
                    UserId = new Guid("2fac849f-ca21-4fa9-bc72-eaa7092bb3a2")
                                    ,
                    ZipCode = "07103"
                }
                , new Models.Contact
                {
                    Id = 2,
                    Birthday = new DateTime(1980, 03, 03),
                    City = "Scranton"
                                    ,
                    Email = "dwightschrute@theofficeus.com",
                    FirstName = "Dwight K.",
                    LastName = "Schrute"
                                    ,
                    PhonePrimary = "345-678-9012",
                    PhoneSecondary = "456-789-0123",
                    State = "PA"
                                    ,
                    StreetAddress1 = "1725 Slough Avenue",
                    StreetAddress2 = "Suite 200 Dunder Mifflin Paper"
                                    ,
                    UserId = new Guid("2fac849f-ca21-4fa9-bc72-eaa7092bb3a2")
                                    ,
                    ZipCode = "07103"
                }
                , new Models.Contact
                {
                    Id = 3,
                    Birthday = new DateTime(1985, 03, 26),
                    City = "Scranton"
                                    ,
                    Email = "JimHalpert@theofficeus.com",
                    FirstName = "Jim ",
                    LastName = "Halpert"
                                    ,
                    PhonePrimary = "987-654-3210",
                    PhoneSecondary = "876-543-2109",
                    State = "PA"
                                    ,
                    StreetAddress1 = "1725 Slough Avenue",
                    StreetAddress2 = "Suite 200 Dunder Mifflin Paper"
                                    ,
                    UserId = new Guid("dcee1c5e-689c-4b76-acb7-75befb803e5b ")
                                    ,
                    ZipCode = "07103-1822"
                }
                , new Models.Contact
                {
                    Id = 4,
                    Birthday = new DateTime(1988, 03, 22),
                    City = "Scranton"
                                    ,
                    Email = "PamBeesly@theofficeus.com",
                    FirstName = "Pam",
                    LastName = "Beesly"
                                    ,
                    PhonePrimary = "765-432-1098",
                    PhoneSecondary = "654-321-0987",
                    State = "PA"
                                    ,
                    StreetAddress1 = "1725 Slough Avenue",
                    StreetAddress2 = "Suite 200 Dunder Mifflin Paper"
                                    ,
                    UserId = new Guid("dcee1c5e-689c-4b76-acb7-75befb803e5b ")
                                    ,
                    ZipCode = "07103-1822"
                }
            );
        }
    }
}
